<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/tradrub_proposee?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_interdit_meme_rubrique' => 'Es ist verboten, eine Übersetzung innerhalb der selben Rubrik wie das Original anzulegen.',
	'erreur_interdit_meme_secteur' => 'Es ist verboten, eine Übersetzung innerhalb der selben Haupt-Rubrik wie das Original anzulegen..',

	// I
	'info_raccourcis' => 'Abkürzungen:',

	// L
	'label_interdit_meme_rubrique' => 'Anlegen von Übersetzungen innerhalb der selben Rubrik verbieten',
	'label_interdit_meme_secteur' => 'Anlegen von Übersetzungen innerhalb der selben Haupt-Rubrik verbieten (eine Haupt-Rubrik pro Sprache)',

	// T
	'titre_tradrub_proposee' => 'Rubriken für Übersetzungen vorschlagen'
);
