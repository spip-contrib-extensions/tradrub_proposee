<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/tradrub_proposee.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_interdit_meme_rubrique' => 'Il est interdit de créer une traduction dans la même rubrique que l’originale.',
	'erreur_interdit_meme_secteur' => 'Il est interdit de créer une traduction dans le même secteur que l’originale.',

	// I
	'info_raccourcis' => 'Raccourcis :',

	// L
	'label_interdit_meme_rubrique' => 'Interdire la création de traduction dans la même rubrique',
	'label_interdit_meme_secteur' => 'Interdire la création de traduction dans le même secteur (un secteur par langue)',

	// T
	'titre_tradrub_proposee' => 'Proposition de rubriques pour les traduction'
);
