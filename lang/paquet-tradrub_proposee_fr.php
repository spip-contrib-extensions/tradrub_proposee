<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/tradrub_proposee.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'tradrub_proposee_description' => 'Ce plugin simplifie le choix de rubrique de destination d’une nouvelle traduction d’article ou de rubrique.',
	'tradrub_proposee_nom' => 'Proposition de rubriques pour les traduction',
	'tradrub_proposee_slogan' => 'Faciliter le choix de rubrique de destination de traductions.'
);
